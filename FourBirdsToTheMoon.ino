#include "defs.h"
#include <SPI.h>
#include <EEPROM.h>
#include "utils.h"

void setup() {
    // put your setup code here, to run once:
    lcdBegin();
    
    delay(500);
    setContrast(60);

    clearDisplay();

    // NOTE: use this to reset EEPROM saved hiscores
    //resethiscores();
}

struct Bird {
    float y;
    float dy;
    char x;
    char points; // theoretical max is about 110
    bool alive;
} p[4];
struct Wall {
    char x;
    char top;
    char bot;
} w[4];


void drawBirds() {
    for(char i = 0; i < 4; i++)
    {
        if(p[i].alive && p[i].y > 2 && p[i].y < LCD_HEIGHT -3) {
            char x = p[i].x;
            char y = p[i].y;
            setPixel(x-2, y);
            setPixel(x-2, y+1);
            setPixel(x-1, y-1);
            setPixel(x-1, y+2);
            setPixel(x  , y-1);
            setPixel(x  , y+1);
            setPixel(x+1, y);
            setPixel(x+1, y+1);
        }
    }
}

void drawWalls() {
    for(char i = 0; i < 4; i++) {
        char x = w[i].x;
        if(x < 2 || x > LCD_WIDTH - 6) {
            continue;
        }
        char t = w[i].top;
        char b = w[i].bot;

        for(int i = 0; i < t; i++) {
            setPixel(x,i);
            setPixel(x+4,i);
        }
        for(int i = b+1; i < LCD_HEIGHT; i++) {
            setPixel(x,i);
            setPixel(x+4,i);
        }
        setPixel(x+1,t);setPixel(x+2,t);setPixel(x+3,t);
        setPixel(x+1,b);setPixel(x+2,b);setPixel(x+3,b);
    }
}

void drawOnePoints(char p, int x, int y) {
    if(p > 99) {
        setChar('g',x,y);
        setChar('g',x+6,y);
    }
    else {
        if(p > 9) {
            setChar((p/10) + '0', x,y);
        }
        setChar((p%10) + '0', x+6,y);
    }
}

void drawAllPoints() {
    drawOnePoints(p[0].points, LCD_WIDTH-28, 2);
    drawOnePoints(p[1].points, LCD_WIDTH-28, LCD_HEIGHT-10);
    drawOnePoints(p[2].points, LCD_WIDTH-14, 2);
    drawOnePoints(p[3].points, LCD_WIDTH-14, LCD_HEIGHT-10);
}

void game() {
    const static int SLOWNESS = 4; // How many cycles of game loop per one pixel of wall movement
    const static float UP_ACCEL = 0.4f; // Speed the birds move after a keypress
    const static float GRAVITY = 0.008f; 
    p[0] = {10,0,4, 0, true};
    p[1] = {10,0,7, 0, true};
    p[2] = {10,0,10, 0, true};
    p[3] = {10,0,13, 0, true};
    w[0] = {40, 5, 20};
    w[1] = {65, 13, 28};
    w[2] = {90, 25, 40};
    w[3] = {115, 11, 26};

    int wallSpeedCounter = 0;
    char nextWallToReplace = 0;
    char latestReplacedWall = 3;
    char wallHoleSize = 15;
    char wallHoleSizeCounter = 4;

    bool keepGoing = true; // true as long as at least one player is alive
    char oldKeys = 0x00; // all are "pressed" so that pressing start does not cause death

    clearDisplay();
    setStr("Ready!", 27, 15);
    updateDisplay();
    delay(600);

    while(keepGoing) {

        if(++wallSpeedCounter == SLOWNESS) {
            for(int i = 0; i < 4; i++) {
                w[i].x -= 1;
            }
            if(w[nextWallToReplace].x == 0) {
                // Wall has reached the end, replace the wall
                if(++wallHoleSizeCounter == 10) {
                    // Decrement the size of the hole every ten walls
                    wallHoleSizeCounter = 0;
                    wallHoleSize--;
                }
                w[nextWallToReplace].x = w[latestReplacedWall].x + 25;
                w[nextWallToReplace].top = random(1,LCD_HEIGHT-1-wallHoleSize);
                w[nextWallToReplace].bot = w[nextWallToReplace].top + wallHoleSize;
                latestReplacedWall = nextWallToReplace;
                nextWallToReplace = (nextWallToReplace + 1) % 4;
            }
            for(int j = 0; j < 4; j++) {
                if(p[j].alive && w[nextWallToReplace].x == p[j].x -1) {
                    p[j].points++;
                }
            }
            wallSpeedCounter = 0;
        }

        char newKeys = PIND;

        char pressed = oldKeys & (~newKeys);

        if(pressed & (1 << P1Pin)) 
            p[0].dy = UP_ACCEL;
        if(pressed & (1 << P2Pin))
            p[1].dy = UP_ACCEL;
        if(pressed & (1 << P3Pin)) 
            p[2].dy = UP_ACCEL;
        if(pressed & (1 << P4Pin)) 
            p[3].dy = UP_ACCEL;

        oldKeys = newKeys;


        for(int i = 0; i < 4; i++) {
            if(p[i].alive) {

                p[i].y -= p[i].dy;
                p[i].dy -= GRAVITY;
                Wall o = w[nextWallToReplace];
                if(p[i].y > LCD_HEIGHT || p[i].y < 0 ||
                        (p[i].x >= o.x && p[i].x < o.x+7 && (p[i].y > o.bot || p[i].y < o.top))) {
                    p[i].alive = false;
                }

            }
        }

        keepGoing = p[0].alive || p[1].alive || p[2].alive || p[3].alive;

        clearDisplay();
        drawBirds();
        drawWalls();
        drawAllPoints();

        updateDisplay();
    }

    delay(700); 
    
    char topListPoints = EEPROM.read(HISCORE_ADDR+11); // third on top list
    for(int i = 0; i < 4; i++) {
        if(p[i].points > topListPoints) {
            newHiScore(i, p[i].points);
            topListPoints = EEPROM.read(HISCORE_ADDR+11); // third on top list, now changed
        }
    }
}

void resethiscores() {
    for(char i = 0; i < 3; i++) {
        EEPROM.write(HISCORE_ADDR+4*i, 'S');
        EEPROM.write(HISCORE_ADDR+4*i+1, 'H');
        EEPROM.write(HISCORE_ADDR+4*i+2, 'I');
        EEPROM.write(HISCORE_ADDR+4*i+3, 6-i);
    }
}

void hiscoremenu() {

    clearDisplay();
  
    setStr("HiScores", 22, 2);
    for(char i = 0; i < 3; i++) {
        setChar(EEPROM.read(HISCORE_ADDR+4*i),2,11+9*i);
        setChar(EEPROM.read(HISCORE_ADDR+4*i+1),8,11+9*i);
        setChar(EEPROM.read(HISCORE_ADDR+4*i+2),14,11+9*i);
        drawOnePoints(EEPROM.read(HISCORE_ADDR+4*i+3), 26, 11+9*i);
    }

    setStr("Back\x19",30,38);  
    updateDisplay();

    while(PIND & (1 << P2Pin)) {} // Wait for the key

    clearDisplay();
}

void moveHiScore(char from, char to) {
    EEPROM.write(HISCORE_ADDR+4*to, EEPROM.read(HISCORE_ADDR+4*from));
    EEPROM.write(HISCORE_ADDR+4*to+1, EEPROM.read(HISCORE_ADDR+4*from+1));
    EEPROM.write(HISCORE_ADDR+4*to+2, EEPROM.read(HISCORE_ADDR+4*from+2));
    EEPROM.write(HISCORE_ADDR+4*to+3, EEPROM.read(HISCORE_ADDR+4*from+3));
}

void setHiScore(char pos, char* name, char points)
{
    EEPROM.write(HISCORE_ADDR+4*pos, name[0]);
    EEPROM.write(HISCORE_ADDR+4*pos+1, name[1]);
    EEPROM.write(HISCORE_ADDR+4*pos+2, name[2]);
    EEPROM.write(HISCORE_ADDR+4*pos+3, points);
}

void newHiScore(char playerNum, char points) {
    clearDisplay();
    setStr("New Hiscore!", 8, 6);
    setChar(PLAYER_CHAR[playerNum], 32, 16);
    drawOnePoints(points, 40, 16);
    setStr("Name:", 8, 30);

    char namebuf[3];
    getName(namebuf);

    if(points > EEPROM.read(HISCORE_ADDR+7)) { // 2nd best score
        moveHiScore(1,2); // second best goes to third best
        if( points > EEPROM.read(HISCORE_ADDR+3)) { // best
            moveHiScore(0,1); // best goes to second best
            setHiScore(0, namebuf, points);
        } else { // new is second best
            setHiScore(1, namebuf, points);
        }
    } else { // new is third best
        setHiScore(2, namebuf, points);
    }
}

void getName(char* buf) {
    buf[0]='A';
    buf[1]='A';
    buf[2]='A';
    byte cur = 0;


    char oldKeys = 0x00; // all are "pressed" so that pressing start does not cause death

    while(cur < 3) {
        setChar(' ', 40, 32); // clear old underlines
        setChar(' ', 46, 32);
        setChar(' ', 52, 32);
        setChar('_', 40 + cur*6, 32); // underline selected char
        setChar(buf[0], 40, 30); // draw chars
        setChar(buf[1], 46, 30);
        setChar(buf[2], 52, 30);
    
        updateDisplay();

        char newKeys = PIND;
    
        char pressed = oldKeys & (~newKeys);

        if(pressed & (1 << P1Pin)) {
            if(cur > 0) cur--;
        }
        if(pressed & (1 << P2Pin)) {
            buf[cur] = 'A' + (buf[cur] - 40) % 26; 
        }    
        if(pressed & (1 << P3Pin)) {
            buf[cur] = 'A' + (buf[cur] - 38) % 26;
        }
        if(pressed & (1 << P4Pin)) {
            cur++;
        }

        oldKeys = newKeys;

    }
    setChar(' ', 40, 32); // clear old underlines
    setChar(' ', 46, 32);
    setChar(' ', 52, 32);
    for(char i = 0; i < 3; i++) {
        setChar(' ', 40, 30); // clear name
        setChar(' ', 46, 30);
        setChar(' ', 52, 30);
        updateDisplay();
        delay(300);
        setChar(buf[0], 40, 30); // draw chars
        setChar(buf[1], 46, 30);
        setChar(buf[2], 52, 30);
        updateDisplay();
        delay(300);
    }
    delay(1000);
    
    clearDisplay();
}


void loop() {
    // put your main code here, to run repeatedly:
    setStr("\x1bHiScore", 2, 17);
    setStr("Play\x1a", LCD_WIDTH-31, 23);
    updateDisplay();
    while(1) {
        char keys = (~PIND) & 0x1B;
        if( keys & (1 << P1Pin)) {
            hiscoremenu();
            break;
        }
        if( keys & (1 << P4Pin)) {
            game();
            break;
        }
    }
}

