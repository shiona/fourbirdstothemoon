#include "defs.h"
#include <SPI.h>

void clearDisplay()
{
    for (int i=0; i<LCD_BYTES; i++)
    {
        displayMap[i] = 0;
    }
}

void setPixel(int x, int y)
{
    byte shift = y % 8;
    displayMap[x + (y/8)*LCD_WIDTH] |= 1<<shift;
}

void clrPixel(int x, int y)
{
    byte shift = y % 8;
    displayMap[x + (y/8)*LCD_WIDTH] &= ~(1<<shift);
}

void setChar(char character, int x, int y)
{
    byte column; // temp byte to store character's column bitmap
    for (int i=0; i<5; i++) // 5 columns (x) per character
    {
        column = ASCII[character][i];
        for (int j=0; j<8; j++) // 8 rows (y) per character
        {
            if (column & (0x01 << j)) // test bits to set pixels
                setPixel(x+i, y+j);
            else
                clrPixel(x+i, y+j);
        }
    }
}

void setStr(char* str, int x, int y)
{
    while(*str != 0) {
        setChar(*str++, x, y);
        x+=6;
    }
}

void lcdWrite(byte data_or_command, byte data)
{
    //Tell the LCD that we are writing either to data or a command
    digitalWrite(dcPin, data_or_command);

    //Send the data
    digitalWrite(scePin, LOW);
    SPI.transfer(data); //shiftOut(sdinPin, sclkPin, MSBFIRST, data);
    digitalWrite(scePin, HIGH);
}


void gotoXY(int x, int y)
{
    lcdWrite(0, 0x80 | x);  // Column.
    lcdWrite(0, 0x40 | y);  // Row.  ?
}

void updateDisplay()
{
    gotoXY(0, 0);
	digitalWrite(dcPin, LCD_DATA);
	digitalWrite(scePin, LOW);

    for (int i=0; i < LCD_BYTES; i++)
    {
        SPI.transfer(displayMap[i]);
    }
	digitalWrite(scePin, HIGH);
}

void setContrast(byte contrast)
{
    lcdWrite(LCD_COMMAND, 0x21); //Tell LCD that extended commands follow
    lcdWrite(LCD_COMMAND, 0x80 | contrast); //Set LCD Vop (Contrast): Try 0xB1(good @ 3.3V) or 0xBF if your display is too dark
    lcdWrite(LCD_COMMAND, 0x20); //Set display mode
}

void lcdBegin(void)
{
    //Configure control pins
    pinMode(scePin, OUTPUT);
    pinMode(rstPin, OUTPUT);
    pinMode(dcPin, OUTPUT);
    pinMode(sdinPin, OUTPUT);
    pinMode(sclkPin, OUTPUT);
    pinMode(blPin, OUTPUT);

    pinMode(P1Pin, INPUT);
    pinMode(P2Pin, INPUT);
    pinMode(P3Pin, INPUT);
    pinMode(P4Pin, INPUT);
    //pinMode(butJoy, INPUT);

    analogWrite(blPin, 0);

    SPI.begin();
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);

    //Reset the LCD to a known state
    digitalWrite(rstPin, LOW);
    digitalWrite(rstPin, HIGH);

    lcdWrite(LCD_COMMAND, 0x21); //Tell LCD extended commands follow
    lcdWrite(LCD_COMMAND, 0xB1); //Set LCD Vop (Contrast)
    lcdWrite(LCD_COMMAND, 0x04); //Set Temp coefficent
    lcdWrite(LCD_COMMAND, 0x14); //LCD bias mode 1:48 (try 0x13)
    //We must send 0x20 before modifying the display control mode
    lcdWrite(LCD_COMMAND, 0x20);
    lcdWrite(LCD_COMMAND, 0x0C); //Set display control, normal mode.  
}
