#ifndef UTILS_H
#define UTILS_H

void clearDisplay();
void setChar(char character, int x, int y);
void setStr(char* str, int x, int y);
void setPixel(int x, int y);
void clrPixel(int x, int y);
void gotoXY(int x, int y);
void updateDisplay();
void setContrast(byte contrast);
void lcdWrite(byte data_or_command, byte data);
void lcdBegin(void);
#endif UTILS_H
